package info.deepidea.shopcommander.android;

/**
 * Created by emix on 6/10/14.
 */
public class ShopListItem {
    private String name;
    private boolean checked;

    public ShopListItem(){ }

    public ShopListItem(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }


}
