package info.deepidea.shopcommander.android;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import java.util.List;


public class ShopItemAdapter extends ArrayAdapter<ShopListItem> {
    private List<ShopListItem> items;
    private ViewHolder viewHolder;


    public ShopItemAdapter(Context context, List<ShopListItem> items) {
        super(context, R.layout.shop_item, items);
        this.items = items;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ShopListItem currentItem = getItem(position);

        viewHolder = null;
        if (convertView == null) {

            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.shop_item, null);
            viewHolder.textView = (TextView) convertView.findViewById(R.id.shopItemTextView);
            viewHolder.checkBox = (CheckBox) convertView.findViewById(R.id.shopItemCheckbox);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.checkBox.setTag(viewHolder.textView);

        viewHolder.checkBox.setOnCheckedChangeListener(null);

        TextView nameView = viewHolder.textView;

        nameView.setText(currentItem.getName());

        togleCheckedListItem(currentItem, nameView);

        viewHolder.checkBox.setChecked(currentItem.isChecked());

        viewHolder.checkBox.setOnCheckedChangeListener((ShoppingListActivity) getContext());

        return convertView;
    }

    public void togleCheckedListItem(ShopListItem currentItem, TextView nameView) {
        if (currentItem.isChecked()){
            nameView.setTypeface(null, Typeface.ITALIC);
            nameView.setPaintFlags(viewHolder.textView.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            nameView.setTextColor(Color.DKGRAY);
        } else {
            nameView.setTypeface(null, Typeface.NORMAL);
            nameView.setPaintFlags(viewHolder.textView.getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));
            nameView.setTextColor(Color.WHITE);
        }
    }

    private static class ViewHolder {
        TextView textView;
        CheckBox checkBox;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public ShopListItem getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
}

