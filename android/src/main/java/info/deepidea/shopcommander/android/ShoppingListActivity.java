package info.deepidea.shopcommander.android;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.*;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ShoppingListActivity extends Activity implements CompoundButton.OnCheckedChangeListener{
    private static final int VOICE_RECOGNITION_REQUEST_CODE = 1001;
    public static final int ACTION_MENU_DETAILS = 1;
    public static final int ACTION_MENU_EDIT    = 2;
    public static final int ACTION_MENU_DELETE  = 3;

    public static final int OPTION_MENU_ABOUT =             1;
    public static final int OPTION_MENU_REMOVE_SELECTED =   2;
    public static final int OPTION_MENU_GENERATE  =         3;

    private List<ShopListItem> items = new ArrayList<>();
    private ListView shoppingList;
    private ShopItemAdapter listAdapter;
    private Button sayButton;
    private ShopListItem currentContextMenuItem;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shopping_list);

        shoppingList = (ListView) findViewById(R.id.shoppingList);
        listAdapter = new ShopItemAdapter(this, items);
        sayButton = (Button) findViewById(R.id.sayButton);

        shoppingList.setAdapter(listAdapter);
        checkVoiceRecognition();

        registerForContextMenu(shoppingList);
        loadSavedItems();
    }

    @Override
    public void onPause(){
        super.onPause();
        saveCurrentState();
    }

    private void loadSavedItems() {
        SharedPreferences sharedPreferences = getSharedPreferences();
        String itemsJson = sharedPreferences.getString(getString(R.string.current_item_list_json), "");
        if (itemsJson.equals("") || itemsJson == null){
            return;
        }
        Gson gson = new Gson();
        try {
            ShopListItem[] listItems = gson.fromJson(itemsJson, ShopListItem[].class);
            if (listItems == null || listItems.length < 1){
                return;
            }

            for (ShopListItem item : listItems){
                listAdapter.add(item);
            }
            listAdapter.setNotifyOnChange(true);
            listAdapter.notifyDataSetChanged();
        }   catch (Throwable e) {
            Log.e("cannot load current list: ", e.toString());
            sharedPreferences.edit().clear();
        }
    }

    private void saveCurrentState() {
        SharedPreferences sharedPreferences = getSharedPreferences();

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        Gson gson = new Gson();
        String jsonStr = gson.toJson(items);

        editor.putString(getString(R.string.current_item_list_json), jsonStr.toString());
        editor.commit();
    }

    private SharedPreferences getSharedPreferences() {
        String CURRENT_ITEM_LIST_SHARED_PREF = getString(R.string.current_item_list);
        return getApplicationContext().getSharedPreferences(CURRENT_ITEM_LIST_SHARED_PREF, Context.MODE_PRIVATE);
    }

    public void listenCommand(View view) {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        // Specify the calling package to identify your application
        intent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE, getClass()
                .getPackage().getName());

        // Display an hint to the user about what he should say.
        String sayMsg = getString(R.string.say_msg);
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT, sayMsg);

        // Given an hint to the recognizer about what the user is going to say
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_WEB_SEARCH);

        int noOfMatches = 1;

        // Specify how many results you want to receive. The results will be
        // sorted where the first result is the one with higher confidence.

        intent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, noOfMatches);

        startActivityForResult(intent, VOICE_RECOGNITION_REQUEST_CODE);
    }

    public void checkVoiceRecognition() {
        // Check if voice recognition is present
        PackageManager pm = getPackageManager();
        List<ResolveInfo> activities = pm.queryIntentActivities(new Intent(
                RecognizerIntent.ACTION_RECOGNIZE_SPEECH), 0);
        if (activities.size() == 0) {
            sayButton.setEnabled(false);
            Toast.makeText(this, "Voice recognizer not present",
                    Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == VOICE_RECOGNITION_REQUEST_CODE)

            //If Voice recognition is successful then it returns RESULT_OK
            if(resultCode == RESULT_OK) {

                ArrayList<String> textMatchList = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);

                if (!textMatchList.isEmpty()) {
                    // If first Match contains the 'search' word
                    // Then start web search.

                    // todo: use VoiceParserStrategy
                    String result = textMatchList.get(0).replace("buy", "");
                    ShopListItem newItem = new ShopListItem(result);

                    addItemToListView(newItem);

                } else {
                    Toast.makeText(this, "Problem: message not found in result", Toast.LENGTH_SHORT).show();
                }
                //Result code for various error.
            }else if(resultCode == RecognizerIntent.RESULT_AUDIO_ERROR){
                showToastMessage("Audio Error");
            }else if(resultCode == RecognizerIntent.RESULT_CLIENT_ERROR){
                showToastMessage("Client Error");
            }else if(resultCode == RecognizerIntent.RESULT_NETWORK_ERROR){
                showToastMessage("Network Error");
            }else if(resultCode == RecognizerIntent.RESULT_NO_MATCH){
                showToastMessage("No Match");
            }else if(resultCode == RecognizerIntent.RESULT_SERVER_ERROR){
                showToastMessage("Server Error");
            }
        super.onActivityResult(requestCode, resultCode, data);
    }

    void showToastMessage(String message){
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    private void addItemToListView(ShopListItem item){
        listAdapter.add(item);
        listAdapter.setNotifyOnChange(true);
        listAdapter.notifyDataSetChanged();
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
        int position = shoppingList.getPositionForView(compoundButton);

        if (position == ListView.INVALID_POSITION) return;

        ShopListItem item = listAdapter.getItem(position);
        item.setChecked(isChecked);
        TextView textView = (TextView) compoundButton.getTag();
        listAdapter.togleCheckedListItem(item, textView);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View view, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, view, menuInfo);
        AdapterView.AdapterContextMenuInfo aInfo = (AdapterView.AdapterContextMenuInfo) menuInfo;

        currentContextMenuItem = listAdapter.getItem(aInfo.position) ;

        menu.setHeaderTitle("what to do?");
        menu.add(1, 1, 1, "Details");
        menu.add(1, 2, 2, "Edit");
        menu.add(1, 3, 3, "Delete");
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        switch (itemId){
            case ACTION_MENU_DELETE:
                if (currentContextMenuItem != null){
                    listAdapter.remove(currentContextMenuItem);
                    listAdapter.notifyDataSetChanged();
                } else {
                    String msg = "error, current context menu item is not set";
                    Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
                    Log.w("ITEM_REMOVE", msg);
                    // todo: refactor
                }
                break;
            default:
                Toast.makeText(this, "Not implemented yet...", Toast.LENGTH_SHORT).show();
        }
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(1, OPTION_MENU_ABOUT, OPTION_MENU_ABOUT, "About");
        menu.add(1, OPTION_MENU_REMOVE_SELECTED, OPTION_MENU_REMOVE_SELECTED, "Remove all selected");
        menu.add(1, OPTION_MENU_GENERATE, OPTION_MENU_GENERATE, "Generate list");
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case OPTION_MENU_ABOUT:
                Toast.makeText(this, "DeepIDEA is the BEST!", Toast.LENGTH_SHORT).show();
                break;
            case OPTION_MENU_GENERATE:
                generateList();
                break;
            case OPTION_MENU_REMOVE_SELECTED:
                removeSelected();
                break;
            default:
                Toast.makeText(this, "not yet implemented", Toast.LENGTH_SHORT).show();
        }

        return true;
    }

    private void removeSelected() {
        List<ShopListItem> itemsCopy = new ArrayList<ShopListItem>(items);
        for (ShopListItem item : itemsCopy){
            if (item.isChecked()){
                listAdapter.remove(item);
            }
        }

        listAdapter.notifyDataSetChanged();
    }

    private void generateList() {
        List<ShopListItem> generatedItems = Arrays.asList(
                new ShopListItem("Potato")
                , new ShopListItem("Tomato")
                , new ShopListItem("Cheese")
                , new ShopListItem("Milk")
                , new ShopListItem("Mineral water")
                , new ShopListItem("Bananas")
                , new ShopListItem("Red onions")
                , new ShopListItem("Red peppers")
                , new ShopListItem("Cucumbers")
                , new ShopListItem("Baby carrots")
                , new ShopListItem("Shredded carrots")
                , new ShopListItem("Celery")
                , new ShopListItem("Romaine lettuce")
                , new ShopListItem("Kiwi")
                , new ShopListItem("Peaches")
                , new ShopListItem("Grapes")
        );
        for (ShopListItem item : generatedItems){
            listAdapter.add(item);
        }
        listAdapter.notifyDataSetChanged();
    }

}
